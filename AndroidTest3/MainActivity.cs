﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using Android.Net;
using Android.Telephony;

namespace AndroidTest3
{
    [Activity(Label = "AndroidTest3", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        //private NetworkConnectivityReceiver mReceiver;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            //mReceiver = new NetworkConnectivityReceiver();
        }

        //protected override void OnResume()
        //{
        //    IntentFilter filter = new IntentFilter(ConnectivityManager.ConnectivityAction);
        //    RegisterReceiver(mReceiver, filter);
        //    base.OnResume();
        //}

        //protected override void OnDestroy()
        //{
        //    UnregisterReceiver(mReceiver);
        //    base.OnPause();
        //}
    }
}

