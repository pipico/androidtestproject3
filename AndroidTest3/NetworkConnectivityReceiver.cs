﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Net;
using Android.Net.Wifi;
using Android.OS;
using Android.Runtime;
using Android.Telephony;
using Android.Views;
using Android.Widget;

namespace AndroidTest3
{
    [BroadcastReceiver(Enabled = true)]
    [IntentFilter (new[] {ConnectivityManager.ConnectivityAction})]
    public class NetworkConnectivityReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            if ((intent != null) && intent.Action == ConnectivityManager.ConnectivityAction)
            {
                Toast.MakeText(context, "Connection Change!", ToastLength.Long).Show();
            }
        }
    }
}
